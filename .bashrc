#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

alias slcok='slock'
#alias mpv='mpv -v'
alias android='qemu-system-x86_64 -enable-kvm -cpu host -m 4096 -smp 4 -usbdevice tablet -soundhw es1370 /mnt/storage/qemu/android.img'
alias qemu='qemu-system-x86_64 -enable-kvm -cpu host -smp 4 -usbdevice tablet'
alias mpd='mpd ~/.config/mpd/mpd.conf'
export PATH=$PATH:~/.scripts
alias lain='mpv --cache=2000 "https://lainchan.org/static/lain.mp3.m3u"'
alias cyber='mpv --cache=2000 "http://cyberadio.pw:8000/stream"'
